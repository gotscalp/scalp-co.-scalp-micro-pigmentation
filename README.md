Here at Scalp Co. SMP we specialize in the art of Scalp Micropigmentation. Using a micro needle we replicate your natural hair follicle pattern so that we may best create the full illusion of a shaved or cropped head of hair.

Address: 717 K St, #503, Sacramento, CA 95814, USA

Phone: 916-793-5404

Website: https://www.gotscalp.com
